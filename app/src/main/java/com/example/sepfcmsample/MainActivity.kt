package com.example.sepfcmsample

import android.app.*
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.app.NotificationCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    // create notification
    private fun createNotification(
        textTitle: String,
        textContent: String,
        map: Map<String, String>
    ) {
        // manager ///////////////////////////////////////////////////////////
        val manager: NotificationManager =
            getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager


        val CHANNEL_ID = "myChannelId"
        val channel_name = "myChannelName"

        // builder ///////////////////////////////////////////////////////////
        var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle(textTitle)
            .setContentText(textContent)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        // pending intent ///////////////////////////////////////////////////////////
        val intent = Intent(this, DetailsActivity::class.java)
        for ((key, value) in map) {
            intent.putExtra(map.get(key), map.get(value))
        }
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        builder.setContentIntent(pendingIntent)


        // create channel ///////////////////////////////////////////////////////////
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            var channel: NotificationChannel =
                NotificationChannel(
                    CHANNEL_ID,
                    channel_name,
                    NotificationManager.IMPORTANCE_DEFAULT
                )

            channel.setShowBadge(true)
            manager.createNotificationChannel(channel)
            builder.setChannelId(CHANNEL_ID)

        }

        manager.notify(1, builder.build())
    }

    fun onclick(view: View) {
        val map = hashMapOf<String, String>()
        createNotification("tittleTest", "bodyTest", map)
    }


}