package com.example.sepfcmsample

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "MyFirebaseMessagingServ"

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        // get data
        val data: Map<String, String> = message.getData()

        // get tittle and body
        val title = message.notification?.title.toString()
        val body = message.notification?.body.toString()

        Log.e(TAG, "onMessageReceived ----> tittle: $title |----> body: $body")

        val map = HashMap<String, String>()

        if (message.data.isNotEmpty()) {

            val name = data.get("name")
            val id = data.get("id")

            Log.e(TAG, "onMessageReceived: name ---> $name | id  ---> $id")

            /*if (name != null && id != null) {
                map.put("name", name)
                map.put("id", id)
            }*/

        }

        createNotification(title, body, map)

    }

    // create notification
    private fun createNotification(
        textTitle: String,
        textContent: String,
        map: Map<String, String>
    ) {
        // manager ///////////////////////////////////////////////////////////
        val manager: NotificationManager =
            getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager


        val CHANNEL_ID = "myChannelId"
        val channel_name = "myChannelName"

        // builder ///////////////////////////////////////////////////////////
        var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle(textTitle)
            .setContentText(textContent)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        // pending intent ///////////////////////////////////////////////////////////
        val intent = Intent(this, DetailsActivity::class.java)
        for ((key, value) in map) {
            intent.putExtra(map.get(key), map.get(value))
        }
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        builder.setContentIntent(pendingIntent)


        // create channel ///////////////////////////////////////////////////////////
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            var channel: NotificationChannel =
                NotificationChannel(
                    CHANNEL_ID,
                    channel_name,
                    NotificationManager.IMPORTANCE_DEFAULT
                )

            channel.setShowBadge(true)
            manager.createNotificationChannel(channel)
            builder.setChannelId(CHANNEL_ID)

        }

        manager.notify(1, builder.build())
    }


    private fun createNotificationChannel() {
        val CHANNEL_ID = "myChannelId"
        val channel_name = "myChannelName"
        val channel_description = "myChannelDescription"

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = channel_name
            val descriptionText = channel_description
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }

}